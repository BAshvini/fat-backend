package com.dfq.fat.purchase.service;

import com.dfq.fat.purchase.entity.Vendor;
import java.util.List;
import java.util.Optional;

public interface VendorService {

    Vendor createVendor(Vendor vendor);

    List<Vendor> getVendors();

    Optional<Vendor> getVendor(long id);
}