package com.dfq.fat.purchase.service;

import com.dfq.fat.purchase.entity.Vendor;
import com.dfq.fat.purchase.repository.VendorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import static java.util.Optional.ofNullable;

@Service
public class VendorServiceImpl implements VendorService{

    @Autowired
    private VendorRepository vendorRepository;

    @Override
    public Vendor createVendor(Vendor vendor) {
        return vendorRepository.save(vendor);
    }

    @Override
    public List<Vendor> getVendors() {
        return vendorRepository.findByActive(true);
    }

    @Override
    public Optional<Vendor> getVendor(long id) {
        return ofNullable(vendorRepository.getOne(id));
    }
}