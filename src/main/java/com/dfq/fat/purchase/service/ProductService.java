package com.dfq.fat.purchase.service;

import com.dfq.fat.purchase.entity.Product;
import java.util.List;
import java.util.Optional;

public interface ProductService {

    Product createProduct(Product product);

    List<Product> getProducts();

    Optional<Product> getProduct(long id);
}