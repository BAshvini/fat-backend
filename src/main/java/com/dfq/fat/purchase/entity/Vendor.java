package com.dfq.fat.purchase.entity;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Setter
@Getter
@Entity
public class Vendor {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    private String name;

    private String phoneNumber;

    private String address;

    private String website;

    private String tinOrTanNumber;

    private String accountNumber;

    private Boolean active;
}