package com.dfq.fat.purchase.entity;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private Date purchaseDate;

    private String productName;

    private String productModel;

    private String description;

    private BigDecimal price;

    private String quantity;

    private String addToBills;

    private String salesTax;
}