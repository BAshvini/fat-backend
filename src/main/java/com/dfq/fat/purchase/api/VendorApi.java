package com.dfq.fat.purchase.api;

import com.dfq.fat.purchase.entity.Vendor;
import com.dfq.fat.purchase.service.VendorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class VendorApi {

    private final VendorService vendorService;

    @Autowired
    private VendorApi(final VendorService vendorService){
        this.vendorService = vendorService;
    }

    @GetMapping("/vendor")
    public ResponseEntity<List<Vendor>> getVendors() {
        List<Vendor> vendors = vendorService.getVendors();
        if (CollectionUtils.isEmpty(vendors)) {
            log.warn("No active vendors found");
            throw new EntityNotFoundException("No vendors found");
        }
        return new ResponseEntity<>(vendors, HttpStatus.OK);
    }

    @PostMapping("/vendor")
    public ResponseEntity<Vendor> createVendor(@Valid @RequestBody Vendor vendor, Principal principal) {

        vendor.setActive(true);
        Vendor persistedObject = vendorService.createVendor(vendor);
        return new ResponseEntity<>(persistedObject, HttpStatus.CREATED);
    }

    @GetMapping("/vendor/{id}")
    public ResponseEntity<Vendor> getVendor(@PathVariable Long id) {

        Optional<Vendor> vendorObj = vendorService.getVendor(id);
        if (!vendorObj.isPresent()) {
            log.warn("Vendor not found : id");
            throw new EntityNotFoundException("vendor not found for id " + id);
        }
        Vendor vendor = vendorObj.get();
        return new ResponseEntity<>(vendor, HttpStatus.OK);
    }
}