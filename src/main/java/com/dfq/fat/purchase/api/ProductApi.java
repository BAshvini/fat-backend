package com.dfq.fat.purchase.api;

import com.dfq.fat.purchase.entity.Product;
import com.dfq.fat.purchase.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class ProductApi {

    private final ProductService productService;

    @Autowired
    private ProductApi(final ProductService productService){
        this.productService = productService;
    }

    @GetMapping("/product")
    public ResponseEntity<List<Product>> getProducts() {
        List<Product> products = productService.getProducts();
        if (CollectionUtils.isEmpty(products)) {
            log.warn("No active products found");
            throw new EntityNotFoundException("No products found");
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @PostMapping("/product")
    public ResponseEntity<Product> createProduct(@Valid @RequestBody Product product, Principal principal) {

        Product persistedObject = productService.createProduct(product);
        return new ResponseEntity<>(persistedObject, HttpStatus.CREATED);
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable long id) {

        Optional<Product> productObj = productService.getProduct(id);
        if (!productObj.isPresent()) {
            log.warn("Product not found : id");
            throw new EntityNotFoundException("Product not found for id " + id);
        }
        Product product = productObj.get();
        return new ResponseEntity<>(product, HttpStatus.OK);
    }
}