package com.dfq.fat.purchase.repository;

import com.dfq.fat.purchase.entity.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface VendorRepository extends JpaRepository<Vendor, Long> {

    List<Vendor> findByActive(Boolean active);
}