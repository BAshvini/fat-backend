package com.dfq.fat.purchase.repository;

import com.dfq.fat.purchase.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

}